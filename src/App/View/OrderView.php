<?php

namespace App\View;

use App\Model\Order\Order;
use Symfony\Component\HttpFoundation\Response;

/**
 * Отображение заказов для frontend
 */
class OrderView
{
    /**
     * Вывод идентификатора
     *
     * @param Order $order Заказ
     * @return Response
     */
    public function showId(Order $order): Response
    {
        return new Response($order->getId());
    }

    /**
     * Вывод статуса заказа
     *
     * @param Order $order Заказ
     * @return Response
     */
    public function showStatus(Order $order): Response
    {
        $message = $order->getStatus() === Order::PAID ? 'ORDER PAID' : 'ORDER NOT PAID';
        
        return new Response($message);
    }
}