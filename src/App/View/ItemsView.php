<?php

namespace App\View;

use App\Model\Item\Item;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Отображение товаров для frontend
 */
class ItemsView
{
    /**
     * Конвертирование в json
     *
     * @param Item[] $items Товары
     * @return JsonResponse
     */
    public function createView(array $items): JsonResponse
    {
        $data = [];
        foreach ($items as $item) {
            $data['items'][] = [
                'Id' => $item->getId(),
                'Name' => $item->getName(),
                'Price' => $item->getPrice()
            ];
        }

        return new JsonResponse($data);
    }
}