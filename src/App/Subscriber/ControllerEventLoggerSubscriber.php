<?php

namespace App\Subscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Логирования контроллеров
 */
class ControllerEventLoggerSubscriber implements EventSubscriberInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * ControllerEventLoggerSubscriber constructor.
     * @param LoggerInterface $logger Логгер
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER_ARGUMENTS => ['onControllersArguments']
        ];
    }

    /**
     * Пишем в лог по событию информацию о контроллере
     *
     * @param ControllerArgumentsEvent $event
     */
    public function onControllersArguments(ControllerArgumentsEvent $event)
    {
        $this->logger->info(
            'Controller {ctrl}, args {args}',
            [
                'ctrl' => get_class($event->getController()),
                'args' => implode(',', $event->getArguments())
            ]
        );
    }
}