<?php

namespace App\Subscriber;

use App\Model\User\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Проверка авторизации
 */
class UserAuthSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserAuthSubscriber constructor.
     * @param UserRepository $userRepository Репозиторий пользователей
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onRequest']
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function onRequest(RequestEvent $event): void
    {
        if ($event->getRequest()->getUser() === null) {
            throw new HttpException(401, 'Unauthorized');
        }

        $user = $this->userRepository->findByLogin($event->getRequest()->getUser());
        if ($user === null) {
            throw new HttpException(401, 'Unauthorized');
        }

        $event->getRequest()->headers->set('PHP_AUTH_USER', 'admin');
    }
}