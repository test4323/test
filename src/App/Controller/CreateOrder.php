<?php

namespace App\Controller;

use App\Model\Order\CreateOrderService;
use App\Model\User\User;
use App\Model\User\UserRepository;
use App\View\OrderView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Создание заказа
 */
class CreateOrder extends AbstractController
{
    /**
     * Создать заказ и вернуть id
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $ids = $request->request->get('ids');
        $ids = array_unique(array_filter(array_map('intval', explode(',', $ids))));

        if (empty($ids)) {
            throw new HttpException(400, 'Empty ids');
        }

        /** @var User $user */
        $user = $this->container->get(UserRepository::class)->findByLogin($request->getUser());

        $order = $this->container->get(CreateOrderService::class)->createOrder($ids, $user->getId());

        return $this->container->get(OrderView::class)->showId($order);
    }
}