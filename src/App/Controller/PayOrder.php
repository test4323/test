<?php

namespace App\Controller;

use App\Model\Order\PayOrderModel;
use App\Model\User\UserRepository;
use App\View\OrderView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PayOrder extends AbstractController
{
    /**
     * Оплатить заказ
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $id = (int)$request->get('id');

        $amount = (float)$request->get('amount');

        $userId = $this->container->get(UserRepository::class)->findByLogin($request->getUser())->getId();

        $order = $this->container->get(PayOrderModel::class)->payAndGetOrder($id, $userId, $amount);

        return $this->container->get(OrderView::class)->showStatus($order);
    }
}