<?php

namespace App\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Всякое полезное для контроллеров
 */
class AbstractController
{
    /**
     * @var ContainerInterface
     */
    protected ContainerInterface $container;

    /**
     * Установить контейнер зависимостей
     *
     * @param ContainerInterface $container Контейнер зависимостей
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }
}