<?php

namespace App\Controller;

use App\Model\Item\CreateItemsService;
use App\View\ItemsView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Генерация стратового набора
 */
class CreateItems extends AbstractController
{
    /**
     * Возвращает 20 товаров (id, название, цена) в json
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        $items = $this->container->get(CreateItemsService::class)->createItems();
        return $this->container->get(ItemsView::class)->createView($items);
    }
}