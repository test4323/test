<?php

namespace App\Model\Payment;

class PaymentTransportDummy extends PaymentTransport
{
    public function getResult(): int
    {
        return 200;
    }
}