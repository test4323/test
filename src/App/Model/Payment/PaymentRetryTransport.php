<?php

namespace App\Model\Payment;

use Exception;

/**
 * Делает несколько попыток вызова, с ожиданием между попытками
 */
class PaymentRetryTransport extends PaymentTransport
{
    /**
     * @var int[]
     */
    private $trys;

    /**
     * PaymentRetryTransport constructor.
     * @param int[] $trys массив попыток с секундами
     */
    public function __construct(array $trys)
    {
        $this->trys = $trys;
    }

    /**
     * Делает несколько попыток вызова, с ожиданием между попытками
     *
     * @return int
     * @throws Exception
     */
    public function getResult(): int
    {
        $trys = $this->trys;
        $trys[] = 0;

        $lastException = null;
        while (!empty($trys)) {
            try {
                return parent::getResult();
            } catch (Exception $e) {
                $lastException = $e;
                $sleep = array_shift($trys);
                sleep($sleep);
            }
        }

        throw $lastException;
    }
}