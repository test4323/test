<?php

namespace App\Model\Payment;

use RuntimeException;

/**
 * HTTP запросы к платёжной системе
 */
class PaymentTransport
{
    /**
     * @var string
     */
    private $url = 'https://ya.ru';

    /**
     * Запрос к платёжной системе
     * Возвращает код http
     *
     * @return int
     */
    public function getResult(): int
    {
        $curl = curl_init($this->url);
        curl_setopt_array(
            $curl,
            [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false,
                CURLOPT_CONNECTTIMEOUT => 300,
                CURLOPT_TIMEOUT => 300,
            ]
        );
        curl_exec($curl);

        $curl_info = curl_getinfo($curl);
        $curl_errno = curl_errno($curl);
        $curl_error = curl_error($curl);
        curl_close($curl);

        if ($curl_errno > 0 || $curl_error) {
            throw new RuntimeException('Curl error #' . $curl_errno . ' ' . $curl_error);
        }

        return (int)$curl_info['http_code'];
    }
}