<?php

namespace App\Model\Payment;

use Exception;

/**
 * Платёжная система
 * Оплата заказов
 */
class Payment
{
    /**
     * @var PaymentTransport
     */
    private $transport;

    /**
     * Payment constructor.
     * @param PaymentTransport $transport Запросы к платёжной системе
     */
    public function __construct(PaymentTransport $transport)
    {
        $this->transport = $transport;
    }

    /**
     * Оплатить заказ через платёжную систему
     * true если платёж проведён успешно
     *
     * @param float $sum Сумма платежа
     * @return bool
     * @throws Exception
     */
    public function pay(float $sum): bool
    {
        return $this->transport->getResult() === 200;
    }
}