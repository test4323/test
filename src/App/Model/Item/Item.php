<?php

namespace App\Model\Item;

use InvalidArgumentException;

/**
 * Товар
 */
class Item
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * Item constructor.
     * @param int $id Идентификатор товара
     * @param string $name Название товара
     * @param float $price Цена товара
     */
    public function __construct(int $id, string $name, float $price)
    {
        if ($id < 0) {
            throw new InvalidArgumentException('Wrong id');
        }

        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * Собрать объект из массива
     *
     * @param array $data Массив с ключами Id, Name, Price
     * @return Item
     */
    public static function fromArray(array $data): self
    {
        return new self($data['Id'], $data['Name'], $data['Price']);
    }

    /**
     * Возвращает идентификатор товара
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Имя товара
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Цена товара
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}