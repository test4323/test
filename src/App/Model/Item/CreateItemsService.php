<?php

namespace App\Model\Item;

/**
 * Генерация и сохранение в бд рандомных товаров
 */
class CreateItemsService
{
    /**
     * @var ItemFactoryInterface
     */
    private $generator;

    /**
     * @var ItemRepository
     */
    private $repository;

    /**
     * @var int
     */
    private $count;

    /**
     * CreateRandomService constructor.
     * @param ItemFactoryInterface $generator Генератор товаров
     * @param ItemRepository $repository Репозиторий товаров
     * @param int $count Сколько товаров сгенерировать
     */
    public function __construct(ItemFactoryInterface $generator, ItemRepository $repository, int $count)
    {
        $this->count = $count;
        $this->generator = $generator;
        $this->repository = $repository;
    }

    /**
     * Создать и записать в бд рандомные товары
     * Возвращает массив товаров
     *
     * @return Item[]
     */
    public function createItems(): array
    {
        $items = [];
        for ($i = 0; $i < $this->count; $i++) {
            $item = $this->generator->create();
            $items[] = $this->repository->save($item);
        }

        return $items;
    }
}