<?php

namespace App\Model\Item;

/**
 * Сравнение товаров
 */
class CompareItems
{
    /**
     * Проверяет расхождение массива идентификаторов с массиво товаров
     * true если массив ids совпадает с массивом ключей items
     *
     * @param int[] $ids Идентификаторы товаров
     * @param Item[]|array<int,Item> $items Товары
     *
     * @return bool
     */
    public function compareIdsWithItems(array $ids, array $items): bool
    {
        $items_ids = array_keys($items);
        $diff1 = array_diff($items_ids, $ids);
        $diff2 = array_diff($ids, $items_ids);

        return empty($diff1) && empty($diff2);
    }
}