<?php

namespace App\Model\Item;

/**
 * Репозиторий товаров
 */
class ItemRepository
{
    /**
     * @var PersistenceInterface
     */
    private $store;

    /**
     * ItemRepository constructor.
     * @param PersistenceInterface $store БД
     */
    public function __construct(PersistenceInterface $store)
    {
        $this->store = $store;
    }

    /**
     * Сохранить товар
     * Возвращает товар с назначенным идентификатором
     *
     * @param Item $item Товар
     * @return Item
     */
    public function save(Item $item): Item
    {
        $id = $this->store->insert(
            $item->getName(),
            $item->getPrice()
        );

        return new Item($id, $item->getName(), $item->getPrice());
    }

    /**
     * Получить массив товаров по их идентификатору
     * id товара будет ключом массива
     *
     * @param int[] $ids
     * @return Item[]
     */
    public function getByIds(array $ids): array
    {
        $ret = [];
        $rawData = $this->store->findByIds($ids);
        foreach ($rawData as $raw) {
            $item = Item::fromArray($raw);
            $ret[$item->getId()] = $item;
        }

        return $ret;
    }
}