<?php

namespace App\Model\Item;

use App\Model\Mysql\AbstractMysql;
use InvalidArgumentException;

/**
 * Запись/чтение товаров в mysql
 */
class MysqlPersistence extends AbstractMysql implements PersistenceInterface
{
    /**
     * @inheritDoc
     */
    public function insert(string $name, float $price): int
    {
        $id = 0;
        $sql = "INSERT INTO `Items` (`Name`, `Price`) VALUES (?,?)";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('sd', $name, $price);
            $stmt->execute();
            $id = $stmt->insert_id;
            $stmt->close();
        }

        $this->watchError();

        return $id;
    }

    /**
     * @inheritDoc
     */
    public function findByIds(array $ids): array
    {
        $ids = array_unique(array_filter(array_map('intval', $ids)));
        if (empty($ids)) {
            throw new InvalidArgumentException('Empty ids');
        }

        $prepare = $this->prepare(count($ids), 'i', '?');

        $ret = [];
        $sql = "SELECT `Id`, `Name`, `Price` FROM `Items` WHERE `Id` IN({$prepare->placeholders})";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param($prepare->types, ...$ids);
            $stmt->bind_result($id, $name, $price);
            if ($stmt->execute()) {
                while ($stmt->fetch()) {
                    $ret[] = ['Id' => (int)$id, 'Name' => $name, 'Price' => (float)$price];
                }
                $stmt->free_result();
            }
            $stmt->close();
        }

        $this->watchError();

        return $ret;
    }
}