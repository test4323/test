<?php

namespace App\Model\Item;

/**
 * Фабрика для генераторов товаров
 */
interface ItemFactoryInterface
{
    /**
     * Создаёт товар
     *
     * @return Item
     */
    public function create(): Item;
}