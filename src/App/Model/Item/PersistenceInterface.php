<?php

namespace App\Model\Item;

/**
 * Интерфейс хранилища
 */
interface PersistenceInterface
{
    /**
     * Добавить товар в бд
     *
     * @param string $name Название товара
     * @param float $price Цена товара
     * @return int
     */
    public function insert(string $name, float $price): int;

    /**
     * Найти товары по идентификаторам
     * Возвращает массив товаров, товар - ['Id' => $id, 'Name' => $name, 'Price' => $price];
     *
     * @param int[] $ids Идентификаторы товаров
     * @return array
     */
    public function findByIds(array $ids): array;
}