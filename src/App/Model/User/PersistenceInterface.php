<?php

namespace App\Model\User;

/**
 * Интерфейс хранилища
 */
interface PersistenceInterface
{
    /**
     * Найти запись по полю Login
     *
     * @param string $login Значение поля Login
     * @return array
     */
    public function findByLogin(string $login): ?array;
}