<?php

namespace App\Model\User;

/**
 * Хранилище пользователей
 */
class InMemoryPersistence implements PersistenceInterface
{
    /**
     * Пользователи
     *
     * @var array
     */
    private $users = [
        'admin' => ['Id' => 1, 'Login' => 'admin']
    ];

    /**
     * @inheritDoc
     */
    public function findByLogin(string $login): ?array
    {
        return $this->users[$login] ?? null;
    }
}