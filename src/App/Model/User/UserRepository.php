<?php

namespace App\Model\User;

/**
 * Репозиторий пользователей
 */
class UserRepository
{
    /**
     * @var PersistenceInterface
     */
    private $persistence;

    /**
     * UserRepository constructor.
     * @param PersistenceInterface $persistence Хранилище пользователей
     */
    public function __construct(PersistenceInterface $persistence)
    {
        $this->persistence = $persistence;
    }

    /**
     * Найти пользователя
     * Всегда возвращает пользователя id=1, name=admin
     *
     * @param string $login Логин пользователя
     * @return User
     */
    public function findByLogin(string $login): ?User
    {
        $userData = $this->persistence->findByLogin($login);

        return $userData === null ? null : User::fromArray($userData);
    }
}