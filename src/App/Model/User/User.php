<?php

namespace App\Model\User;

/**
 * Класс пользователя
 */
class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $login;

    /**
     * User constructor.
     * @param int $id Идентификатор пользователя
     * @param string $login Имя пользователя
     */
    public function __construct(int $id, string $login)
    {
        $this->id = $id;
        $this->login = $login;
    }

    /**
     * Собрать объект из массива
     *
     * @param array $data Массив данных пользователя
     * @return static
     * @internal
     */
    public static function fromArray(array $data): self
    {
        return new self((int)$data['Id'], $data['Login']);
    }

    /**
     * Идентификатор пользователя
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Имя пользователя
     *
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }
}