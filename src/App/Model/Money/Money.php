<?php

namespace App\Model\Money;

/**
 * Работа с деньгами
 */
class Money
{
    /**
     * @var int множитель
     */
    private $microamount = 1000000;

    /**
     * Пробразовать float в int microamount
     *
     * @param float $amount Сумма денег
     * @return int
     */
    public function toMicroAmount(float $amount): int
    {
        return ceil($amount * $this->microamount);
    }
}