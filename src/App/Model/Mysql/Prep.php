<?php

namespace App\Model\Mysql;

class Prep
{
    /**
     * Строка типов
     *
     * @var string
     */
    public string $types;

    /**
     * Массив подстановок
     *
     * @var string
     */
    public string $placeholders;

    /**
     * Массив значений для подстановок
     *
     * @var array
     */
    public array $params;

    /**
     * Prep constructor.
     * @param string $types Строка типов
     * @param string $placeholders Массив подстановок
     * @param array $params Массив значений для подстановок
     */
    public function __construct(string $types, string $placeholders, array $params)
    {
        $this->types = $types;
        $this->placeholders = $placeholders;
        $this->params = $params;
    }
}
