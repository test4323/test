<?php

namespace App\Model\Mysql;

use Closure;
use mysqli;
use RuntimeException;
use Throwable;

/**
 * Абстрактный класс для всех хранилищ mysql
 */
class AbstractMysql
{
    /**
     * @var mysqli
     */
    protected $db;

    /**
     * ItemStore constructor.
     * @param mysqli $db Инстанс mysqli
     */
    public function __construct(mysqli $db)
    {
        $this->db = $db;
    }

    /**
     * Выполнить транзакцию
     *
     * @param Closure $callback Операции транзакции
     * @return mixed
     * @throws Throwable
     */
    public function transaction(Closure $callback)
    {
        $this->db->query("START TRANSACTION");

        try {
            $result = $callback();
            $this->db->query("COMMIT");
        } catch (Throwable $e) {
            $this->db->query("ROLLBACK");
            throw $e;
        }

        return $result;
    }

    /**
     * Проверка наличия ошибки
     */
    protected function watchError(): void
    {
        if (!empty($this->db->error)) {
            throw new RuntimeException('Db error: ' . $this->db->error);
        }
    }

    /**
     * Помошник для prepared statement
     * Возвращает массив, где по ключу placeholders - строка подстановок, types - строка типов подстановок, params - значения
     *
     * @param int $count Количество элементов
     * @param string $typeTpl Шаблон типа
     * @param string $placeholderTpl шаблон плейсхолдера
     * @param Closure $bindParams Формирование параметров для bind_params
     * @return Prep
     */
    protected function prepare(int $count, string $typeTpl, string $placeholderTpl, ?Closure $bindParams = null): Prep
    {
        $placeholders = [];
        $types = '';
        $params = [];
        for ($i = 0; $i < $count; $i++) {
            $placeholders[] = $placeholderTpl;
            $types .= $typeTpl;

            if ($bindParams !== null) {
                $bindParams($params, $i);
            }
        }

        return new Prep($types, implode(',', $placeholders), $params);
    }
}