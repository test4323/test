<?php

namespace App\Model\Order;

use App\Model\Item\CompareItems;
use App\Model\Item\ItemRepository;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Создание заказа
 */
class CreateOrderService
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CompareItems
     */
    private $compareItems;

    /**
     * CreateOrderModel constructor.
     * @param ItemRepository $itemRepository Репозиторий товаров
     * @param OrderRepository $orderRepository Репозиторий заказов
     * @param CompareItems $compareItems Сравнение товаров
     */
    public function __construct(
        ItemRepository $itemRepository,
        OrderRepository $orderRepository,
        CompareItems $compareItems
    ) {
        $this->itemRepository = $itemRepository;
        $this->orderRepository = $orderRepository;
        $this->compareItems = $compareItems;
    }

    /**
     * Создать заказ и сохранить в бд
     * Возвращает заказ
     *
     * @param int[] $ids Идентификаторы товаров
     * @param int $userId Идентификатор пользователя
     * @return Order
     * @throws Throwable
     */
    public function createOrder(array $ids, int $userId): Order
    {
        $items = $this->itemRepository->getByIds($ids);

        if (!$this->compareItems->compareIdsWithItems($ids, $items)) {
            throw new HttpException(400, 'Some items not found');
        }

        $order = new Order(0, Order::NEW, $ids, $userId);

        $order = $this->orderRepository->save($order);

        return $order;
    }
}