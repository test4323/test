<?php

namespace App\Model\Order;

use App\Model\Mysql\AbstractMysql;
use RuntimeException;

/**
 * Запись/чтение заказов в mysql
 */
class MysqlPersistence extends AbstractMysql implements PersistenceInterface
{
    /**
     * @inheritDoc
     */
    public function insertOrder(int $userId, int $status): int
    {
        $id = 0;
        $sql = "INSERT INTO `Orders` (`UserId`, `Status`) VALUES (?,?)";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('ii', $userId, $status);
            if ($stmt->execute()) {
                $id = $stmt->insert_id;
            }
            $stmt->close();
        }

        if ($id === 0 || !empty($this->db->error)) {
            throw new RuntimeException('Db error: ' . $this->db->error);
        }

        return $id;
    }

    /**
     * @inheritDoc
     */
    public function insertIds(int $id, array $ids): void
    {
        $function = function (&$params, $i) use ($id, $ids) {
            $params[] = $id;
            $params[] = $ids[$i];
        };
        $prepare = $this->prepare(count($ids), 'ii', '(?,?)', $function);

        $sql = "INSERT INTO OrdersItems (OrderId, ItemId) VALUES " . $prepare->placeholders;
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param($prepare->types, ...$prepare->params);
            $stmt->execute();
            $stmt->close();
        }

        $this->watchError();
    }

    /**
     * @inheritDoc
     */
    public function getOrderById(int $id): ?array
    {
        $ret = null;
        $sql = "SELECT `UserId`, `Status` FROM `Orders` WHERE `Id` = ? LIMIT 1";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $id);
            if ($stmt->execute()) {
                $stmt->bind_result($userId, $status);
                while ($stmt->fetch()) {
                    $ret = ['UserId' => $userId, 'Status' => $status];
                }
                $stmt->free_result();
            }
            $stmt->close();
        }

        $this->watchError();

        return $ret;
    }

    /**
     * @inheritDoc
     */
    public function getItemsIdsById(int $id): array
    {
        $sql = "SELECT `ItemId` FROM `OrdersItems` WHERE `OrderId` = ?";
        $ids = [];
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('i', $id);
            if ($stmt->execute()) {
                $stmt->bind_result($id);
                while ($stmt->fetch()) {
                    $ids[] = (int)$id;
                }
                $stmt->free_result();
            }
            $stmt->close();
        }

        $this->watchError();

        return $ids;
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, int $status): void
    {
        $sql = "UPDATE `Orders` SET `Status` = ? WHERE Id = ?";
        if ($stmt = $this->db->prepare($sql)) {
            $stmt->bind_param('ii', $status, $id);
            $stmt->execute();
            $stmt->close();
        }

        $this->watchError();
    }
}