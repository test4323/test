<?php

namespace App\Model\Order;

use InvalidArgumentException;

/**
 * Заказ
 */
class Order
{
    /**
     * Статус: новый
     *
     * @var int
     */
    public const NEW = 1;

    /**
     * Статус: оплаченный
     *
     * @var int
     */
    public const PAID = 2;

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $status;

    /**
     * @var int[]
     */
    private $itemsIds;

    /**
     * @var int
     */
    private $userId;

    /**
     * Order constructor.
     * @param int $id Идентификатор заказа
     * @param int $status Статус заказа
     * @param int[] $itemsIds Идентификаторы товаров в заказе
     * @param int $userId Идентификатор пользователя
     */
    public function __construct(int $id, int $status, array $itemsIds, int $userId)
    {
        if ($id < 0) {
            throw new InvalidArgumentException('Wrong id');
        }

        $this->setStatus($status);

        $this->id = $id;
        $this->itemsIds = $itemsIds;
        $this->userId = $userId;
    }

    /**
     * Возвращает идентификатор заказа
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Установить статус
     *
     * @param int $status Статус
     */
    public function setStatus(int $status): void
    {
        if ($status !== self::NEW && $status !== self::PAID) {
            throw new InvalidArgumentException('Unknown status');
        }

        $this->status = $status;
    }

    /**
     * Возвращает статус заказа
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Возвращает идентификаторы товаров
     *
     * @return int[]
     */
    public function getItemsIds(): array
    {
        return $this->itemsIds;
    }

    /**
     * Возвращает идентификатор пользователя
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}