<?php

namespace App\Model\Order;

use Throwable;

/**
 * Репозиторий заказов
 */
class OrderRepository
{
    /**
     * @var PersistenceInterface
     */
    private $store;

    /**
     * Repository constructor.
     * @param PersistenceInterface $store БД
     */
    public function __construct(PersistenceInterface $store)
    {
        $this->store = $store;
    }

    /**
     * Сохранить заказ
     *
     * @param Order $order Заказ
     * @return Order
     * @throws Throwable
     */
    public function save(Order $order): Order
    {
        if ($order->getId() > 0) {
            $this->store->update($order->getId(), $order->getStatus());
        } else {
            $id = $this->store->transaction(
                function () use ($order) {
                    $id = $this->store->insertOrder($order->getUserId(), $order->getStatus());
                    $this->store->insertIds($id, $order->getItemsIds());
                    return $id;
                }
            );
            $order = new Order($id, $order->getStatus(), $order->getItemsIds(), $order->getUserId());
        }

        return $order;
    }

    /**
     * Получить заказ по id
     *
     * @param int $id Идентификатор заказа
     * @return Order
     */
    public function getById(int $id): ?Order
    {
        $orderData = $this->store->getOrderById($id);

        if ($orderData === null) {
            return null;
        }

        $ids = $this->store->getItemsIdsById($id);

        return new Order($id, $orderData['Status'], $ids, $orderData['UserId']);
    }
}