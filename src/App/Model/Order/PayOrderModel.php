<?php

namespace App\Model\Order;

use App\Model\Item\CompareItems;
use App\Model\Item\Item;
use App\Model\Item\ItemRepository;
use App\Model\Money\Money;
use App\Model\Payment\Payment;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Оплата заказа
 */
class PayOrderModel
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var Money
     */
    private $money;
    /**
     * @var Payment
     */
    private $payment;
    /**
     * @var CompareItems
     */
    private $compareItems;

    /**
     * PayOrderModel constructor.
     * @param OrderRepository $orderRepository Репозиторий заказов
     * @param ItemRepository $itemRepository Рпозиторий товаров
     * @param Money $money Утилита для работы с деньгами
     * @param Payment $payment Платёжная система
     * @param CompareItems $compareItems Сравнение товаров
     */
    public function __construct(
        OrderRepository $orderRepository,
        ItemRepository $itemRepository,
        Money $money,
        Payment $payment,
        CompareItems $compareItems
    ) {
        $this->orderRepository = $orderRepository;
        $this->itemRepository = $itemRepository;
        $this->money = $money;
        $this->payment = $payment;
        $this->compareItems = $compareItems;
    }

    /**
     * Оплачивает и возвращает заказ
     *
     * @param int $orderId Идентификатор заказа
     * @param int $userId Идентификатор пользователя
     * @param float $payAmount Сумма на оплату
     * @return Order
     * @throws Throwable
     */
    public function payAndGetOrder(int $orderId, int $userId, float $payAmount): Order
    {
        $this->checkArguments($orderId, $payAmount);

        $order = $this->getOrderAndCheck($orderId, $userId);

        $items = $this->getItemsAndCheck($order->getItemsIds());

        $sum = $this->getItemsPriceSum($items);

        if ($sum !== $this->money->toMicroAmount($payAmount)) {
            throw new HttpException(400, 'Pay amount not equal price sum');
        }

        if ($this->payment->pay($sum)) {
            $order->setStatus(Order::PAID);
            $this->orderRepository->save($order);
        }

        return $order;
    }

    /**
     * Суммарная стоимость товаров в заказе
     *
     * @param Item[] $items
     * @return int
     */
    private function getItemsPriceSum(array $items): int
    {
        $sum = 0;
        foreach ($items as $item) {
            $sum += $this->money->toMicroAmount($item->getPrice());
        }

        return $sum;
    }

    /**
     * Проверка аргументов
     *
     * @param int $orderId Идентификатор заказа
     * @param float $payAmount Сумма на оплату
     */
    private function checkArguments(int $orderId, float $payAmount): void
    {
        if ($orderId <= 0) {
            throw new HttpException(400, 'Wrong order id');
        }

        if ($payAmount <= 0) {
            throw new HttpException(400, 'Wrong pay amount');
        }
    }

    /**
     * Найти заказ и проверить состояния
     *
     * @param int $orderId Идентификатор заказа
     * @param int $userId Идентификатор пользователя
     * @return Order
     */
    private function getOrderAndCheck(int $orderId, int $userId): Order
    {
        $order = $this->orderRepository->getById($orderId);

        if ($order === null) {
            throw new HttpException(400, 'Order not found');
        }

        if ($order->getUserId() !== $userId) {
            throw new HttpException(400, 'Order not found');
        }

        if (empty($order->getItemsIds())) {
            throw new HttpException(400, 'Order without items');
        }

        if ($order->getStatus() === Order::PAID) {
            throw new HttpException(400, 'Order already paid');
        }

        return $order;
    }

    /**
     * Найти и проверить товары в заказе
     *
     * @param int[] $itemsIds
     * @return Item[]
     */
    private function getItemsAndCheck(array $itemsIds): array
    {
        $items = $this->itemRepository->getByIds($itemsIds);
        if (empty($items)) {
            throw new HttpException(400, 'Empty items list');
        }

        if (!$this->compareItems->compareIdsWithItems($itemsIds, $items)) {
            throw new HttpException(400, 'Some items not found');
        }

        return $items;
    }
}