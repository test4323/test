<?php

namespace App\Model\Order;

use Closure;
use Throwable;

/**
 * Интерфейс хранилища
 */
interface PersistenceInterface
{
    /**
     * Добавить заказ в бд
     * Возвращает идентификатор созданного заказа
     *
     * @param int $userId Идентификатор пользователя
     * @param int $status Статус заказа
     * @return int
     */
    public function insertOrder(int $userId, int $status): int;

    /**
     * Добавить в заказ идентификаторы товаров
     *
     * @param int $id Идентификатор заказа
     * @param array $ids Идентификаторы товаров
     * @return void
     */
    public function insertIds(int $id, array $ids): void;

    /**
     * Получить параметры заказа по идентификатору
     *
     * @param int $id Идентификатор заказа
     * @return array|null
     */
    public function getOrderById(int $id): ?array;

    /**
     * Получить товары по идентификатору заказа
     *
     * @param int $id Идентификатор заказа
     * @return array
     */
    public function getItemsIdsById(int $id): array;

    /**
     * Обновить значения заказа
     * @param int $id Идентификатор задака
     * @param int $status Статус заказа
     */
    public function update(int $id, int $status): void;

    /**
     * Выполнить транзакцию
     *
     * @param Closure $callback Операции транзакции
     * @return mixed
     * @throws Throwable
     */
    public function transaction(Closure $callback);
}