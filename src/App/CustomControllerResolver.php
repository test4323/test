<?php

namespace App;

use App\Controller\AbstractController;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;

/**
 * Резолвер контроллеров, добавляющи контроллерам контейнер зависимостей
 */
class CustomControllerResolver extends ControllerResolver
{
    /**
     * @var ContainerInterface
     */
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container, LoggerInterface $logger = null)
    {
        parent::__construct($logger);
        $this->container = $container;
    }

    /**
     * Добавление контейнера в контроллер
     *
     * @inheritDoc
     */
    protected function instantiateController(string $class)
    {
        $class = new $class();
        if ($class instanceof AbstractController) {
            $class->setContainer($this->container);
        }

        return $class;
    }
}