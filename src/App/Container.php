<?php

namespace App;

use App\Model\Item\CreateItemsService;
use App\Model\Order\CreateOrderService;
use App\Model\Order\PayOrderModel;
use App\Model\Item\CompareItems;
use App\Model\Item\ItemFactory;
use App\Model\Item\ItemFactoryInterface;
use App\Model\Item\ItemRepository;
use App\Model\Item\MysqlPersistence;
use App\Model\Money\Money;
use App\Model\Order\MysqlPersistence as OrderMysqlPersistence;
use App\Model\Order\OrderRepository;
use App\Model\Payment\Payment;
use App\Model\Payment\PaymentRetryTransport;
use App\Model\Payment\PaymentTransportDummy;
use App\Model\User\InMemoryPersistence;
use App\Model\User\UserRepository;
use App\Subscriber\ControllerEventLoggerSubscriber;
use App\View\ItemsView;
use App\View\OrderView;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Processor\PsrLogMessageProcessor;
use mysqli;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Найстройка контейнера
 */
class Container
{
    /** @var bool */
    private $prod;

    public function __construct(bool $prod = true)
    {
        $this->prod = $prod;
    }

    /**
     * Возвращает билдер контейнера
     *
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        $containerBuilder = new ContainerBuilder();

        if ($this->prod) {
            $this->db($containerBuilder);
        } else {
            $this->dbDev($containerBuilder);
        }

        $this->loggers($containerBuilder);
        $this->users($containerBuilder);
        $this->items($containerBuilder);
        $this->orders($containerBuilder);

        $this->models($containerBuilder);
        $this->views($containerBuilder);
        $this->subscribers($containerBuilder);

        return $containerBuilder;
    }

    /**
     * Настрока бд
     *
     * @param ContainerBuilder $containerBuilder
     */
    protected function db(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(mysqli::class, mysqli::class)
            ->addArgument('mysql')
            ->addArgument('root')
            ->addArgument('123456')
            ->addArgument('vse');
    }

    /**
     * Настрока бд !prod
     *
     * @param ContainerBuilder $containerBuilder
     */
    protected function dbDev(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(mysqli::class, mysqli::class)
            ->addArgument('mysql')
            ->addArgument('root')
            ->addArgument('123456')
            ->addArgument('vse.test');
    }

    /**
     * Настрока логеров
     *
     * @param ContainerBuilder $containerBuilder Билдер контейнера
     */
    private function loggers(ContainerBuilder $containerBuilder): void
    {
        $loggerFormatter = new LineFormatter("[%datetime%] %level_name%: %message%\n");
        $loggerFormatter->includeStacktraces(true);
        $errorLogger = (new Logger('ERROR'))
            ->pushProcessor(new PsrLogMessageProcessor())
            ->pushHandler(
                (new ErrorLogHandler(ErrorLogHandler::SAPI))->setFormatter($loggerFormatter)
            );
        $containerBuilder->set('ErrorLogger', $errorLogger);

        $eventLogger = new Logger('EVENT');
        $eventLogger
            ->pushProcessor(new PsrLogMessageProcessor())
            ->pushHandler(
                (new RotatingFileHandler(__DIR__ . '/../../log/event.log', 5))->setFormatter($loggerFormatter)
            );

        $containerBuilder->set('EventLogger', $eventLogger);
    }

    /**
     * Настрока сервисов пользователя
     *
     * @param ContainerBuilder $containerBuilder Билдер контейнера
     */
    private function users(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(UserRepository::class, UserRepository::class)
            ->addArgument(new InMemoryPersistence());
    }

    /**
     * Настрока сервисов товаров
     *
     * @param ContainerBuilder $containerBuilder Билдер
     */
    private function items(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(ItemFactoryInterface::class, ItemFactory::class);

        $containerBuilder->register(MysqlPersistence::class, MysqlPersistence::class)
            ->addArgument(new Reference(mysqli::class));

        $containerBuilder->register(ItemRepository::class, ItemRepository::class)
            ->addArgument(new Reference(MysqlPersistence::class));

        $containerBuilder->register(CompareItems::class, CompareItems::class);
    }

    /**
     * Настрока ссервисов заказов
     *
     * @param ContainerBuilder $containerBuilder Билдер
     */
    private function orders(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(OrderMysqlPersistence::class, OrderMysqlPersistence::class)
            ->addArgument(new Reference(mysqli::class));
        $containerBuilder->register(OrderRepository::class, OrderRepository::class)
            ->addArgument(new Reference(OrderMysqlPersistence::class));
    }

    /**
     * НГастрока моделей
     *
     * @param ContainerBuilder $containerBuilder Билдер
     */
    private function models(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(CreateItemsService::class, CreateItemsService::class)
            ->addArgument(new Reference(ItemFactoryInterface::class))
            ->addArgument(new Reference(ItemRepository::class))
            ->addArgument(20);

        $containerBuilder->register(CreateOrderService::class, CreateOrderService::class)
            ->addArgument(new Reference(ItemRepository::class))
            ->addArgument(new Reference(OrderRepository::class))
            ->addArgument(new Reference(CompareItems::class));

        $containerBuilder->register(PayOrderModel::class, PayOrderModel::class)
            ->addArgument(new Reference(OrderRepository::class))
            ->addArgument(new Reference(ItemRepository::class))
            ->addArgument(new Money())
            ->addArgument(new Payment($this->prod ? new PaymentRetryTransport([1, 2, 3]) : new PaymentTransportDummy()))
            ->addArgument(new Reference(CompareItems::class));
    }

    /**
     * Настройка представлений
     *
     * @param ContainerBuilder $containerBuilder Билдер
     */
    private function views(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(ItemsView::class, ItemsView::class);
        $containerBuilder->register(OrderView::class, OrderView::class);
    }

    /**
     * Настрока подписчиков
     *
     * @param ContainerBuilder $containerBuilder Билдер
     */
    private function subscribers(ContainerBuilder $containerBuilder): void
    {
        $containerBuilder->register(ControllerEventLoggerSubscriber::class, ControllerEventLoggerSubscriber::class)
            ->addArgument(new Reference('EventLogger'));
    }
}