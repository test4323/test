<?php

namespace App;

use App\Controller\CreateItems;
use App\Controller\CreateOrder;
use App\Controller\PayOrder;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Роуты
 */
class Routes
{
    /**
     * Настройка роутов
     *
     * @return RouteCollection
     */
    public function init(): RouteCollection
    {
        $routes = new RouteCollection();

        $routes->add(
            'create-items',
            new Route(
                '/api/items/create',
                ['_controller' => CreateItems::class],
                [], [], null, [], 'GET'
            )
        );
        $routes->add(
            'create-order',
            new Route(
                '/api/order/create',
                ['_controller' => CreateOrder::class],
                ['ids' => '(\d,)+\d'],
                [],
                null,
                [],
                'POST'
            )
        );
        $routes->add(
            'pay-order',
            new Route(
                '/api/order/pay',
                ['_controller' => PayOrder::class],
                ['id' => '\d+', 'amount' => '\d+\d*\.?\d*'],
                [],
                null,
                [],
                'POST'
            )
        );

        return $routes;
    }
}