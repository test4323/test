<?php

namespace App;

use App\Model\User\UserRepository;
use App\Subscriber\ControllerEventLoggerSubscriber;
use App\Subscriber\ExceptionSubscriber;
use App\Subscriber\LoggerSubscriber;
use App\Subscriber\EmulateUserAuthSubscriber;
use App\Subscriber\UserAuthSubscriber;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

/**
 * Настройка приложения
 */
class App
{
    /**
     * Продакшен или нет
     * @var bool
     */
    private $prod = false;

    public function __construct(string $env)
    {
        $this->prod = $env === 'prod';
    }

    /**
     * Запустить приложение
     *
     * @throws Exception
     */
    public function run(): void
    {
        $container = (new Container($this->prod))->getContainer();
        $routes = (new Routes())->init();

        $matcher = new UrlMatcher($routes, new RequestContext());

        $errorLogger = $container->get('ErrorLogger');

        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));
        $dispatcher->addSubscriber(new LoggerSubscriber($errorLogger));
        $dispatcher->addSubscriber(new ExceptionSubscriber());
        $dispatcher->addSubscriber($container->get(ControllerEventLoggerSubscriber::class));
        $dispatcher->addSubscriber(new EmulateUserAuthSubscriber());
        $dispatcher->addSubscriber(new UserAuthSubscriber($container->get(UserRepository::class)));

        $controllerResolver = new CustomControllerResolver($container);
        $argumentResolver = new ArgumentResolver();

        $kernel = new HttpKernel($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);

        $request = Request::createFromGlobals();
        $response = $kernel->handle($request);
        $response->send();

        $kernel->terminate($request, $response);
    }
}