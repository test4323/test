<?php

require_once __DIR__ . '/vendor/autoload.php';

$env = file_get_contents(__DIR__ . '/.env');
(new \App\App($env))->run();