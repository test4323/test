<?php

namespace functional;

use App\Container;
use App\Model\Item\CreateItemsService;
use App\Model\Order\CreateOrderService;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CreateOrderModelTest extends Unit
{
    /** @var \FunctionalTester */
    protected $tester;
    /** @var ContainerInterface */
    protected $container;

    protected function _before()
    {
        $this->container = (new Container(false))->getContainer();
    }

    /**
     * @test
     */
    public function createItems()
    {
        $this->container->get(CreateItemsService::class)->createItems();

        $this->container->get(CreateOrderService::class)->createOrder([1, 2, 3], 1);

        $this->tester->seeNumRecords(1, 'Orders');
        $this->tester->seeInDatabase('Orders', ['Id' => 1, 'UserId' => 1, 'Status' => 1]);

        $this->tester->seeNumRecords(3, 'OrdersItems');
        $this->tester->seeInDatabase('OrdersItems', ['OrderId' => 1, 'ItemId' => 1]);
        $this->tester->seeInDatabase('OrdersItems', ['OrderId' => 1, 'ItemId' => 2]);
        $this->tester->seeInDatabase('OrdersItems', ['OrderId' => 1, 'ItemId' => 3]);
    }
}