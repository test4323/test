<?php

namespace functional;

use App\Container;
use App\Model\Item\CreateItemsService;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CreateItemsModelTest extends Unit
{
    /** @var \FunctionalTester */
    protected $tester;
    /** @var ContainerInterface */
    protected $container;

    protected function _before()
    {
        $this->container = (new Container(false))->getContainer();
    }

    /**
     * @test
     */
    public function createItems()
    {
        $this->container->get(CreateItemsService::class)->createItems();
        $this->tester->seeNumRecords(20, 'Items');
    }
}