<?php

namespace functional;

use App\Container;
use App\Model\Item\CreateItemsService;
use App\Model\Order\CreateOrderService;
use App\Model\Order\PayOrderModel;
use Codeception\Test\Unit;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PayOrderModelTest extends Unit
{
    /** @var \FunctionalTester */
    protected $tester;
    /** @var ContainerInterface */
    protected $container;

    protected function _before()
    {
        $this->container = (new Container(false))->getContainer();
    }

    /**
     * @test
     */
    public function getOrder()
    {
        $this->container->get(CreateItemsService::class)->createItems();

        $this->container->get(CreateOrderService::class)->createOrder([1, 2, 3], 1);

        $this->container->get(PayOrderModel::class)->payAndGetOrder(1, 1, 368.55);

        $this->tester->seeInDatabase('Orders', ['Id' => 1, 'UserId' => 1, 'Status' => 2]);
    }
}