<?php

namespace api;

class CreateOrderCest
{
    public function createOrder(\ApiTester $I): void
    {
        $I->wantTo('create start items');
        $I->sendGET('/api/items/create');

        $I->wantTo('create order with items');
        $I->sendPOST('/api/order/create', ['ids' => '1,2,3']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains(1);
    }

    public function createOrderItemsNotFound(\ApiTester $I): void
    {
        $I->wantTo('create order with items, but items not exist');
        $I->sendPOST('/api/order/create', ['ids' => '1,2,3']);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('Some items not found');
    }
}