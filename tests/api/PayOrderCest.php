<?php

namespace api;

class PayOrderCest
{
    public function _before(\ApiTester $I): void
    {
        $I->wantTo('create start items');
        $I->sendGET('/api/items/create');
    }

    public function payOrder(\ApiTester $I): void
    {
        $I->wantTo('create order with items');
        $I->sendPOST('/api/order/create', ['ids' => '1,2,3']);

        $I->wantTo('pay order');
        $I->sendPOST('/api/order/pay', ['id' => 1, 'amount' => 368.55]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains('ORDER PAID');
    }

    public function payOrderNotFound(\ApiTester $I): void
    {
        $I->wantTo('create order with items, but order not found');
        $I->sendPOST('/api/order/create', ['ids' => '1,2,3']);

        $I->wantTo('pay order');
        $I->sendPOST('/api/order/pay', ['id' => 2, 'amount' => 368.55]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('Order not found');
    }

    public function payOrderWithoutOrder(\ApiTester $I): void
    {
        $I->wantTo('pay order without order id');
        $I->sendPOST('/api/order/pay', ['amount' => 368.55]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseContains('Wrong order id');
    }
}