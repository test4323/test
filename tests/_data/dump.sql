
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `vse.test`
--
CREATE DATABASE IF NOT EXISTS `vse.test` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `vse.test`;

-- --------------------------------------------------------

--
-- Структура таблицы `Items`
--

DROP TABLE IF EXISTS `Items`;
CREATE TABLE IF NOT EXISTS `Items` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Название товара',
  `Price` float UNSIGNED NOT NULL COMMENT 'Цена товара',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Все товары системы';

-- --------------------------------------------------------

--
-- Структура таблицы `Orders`
--

DROP TABLE IF EXISTS `Orders`;
CREATE TABLE IF NOT EXISTS `Orders` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `UserId` int NOT NULL COMMENT 'Id пользователя',
  `Status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Статус: 0 - новый, 1 - оплачен',
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`,`UserId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `OrdersItems`
--

DROP TABLE IF EXISTS `OrdersItems`;
CREATE TABLE IF NOT EXISTS `OrdersItems` (
  `OrderId` int NOT NULL,
  `ItemId` int NOT NULL,
  UNIQUE KEY `OrderId` (`OrderId`,`ItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
