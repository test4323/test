<?php

namespace unit\App\Model\Order;

use App\Model\Order\Order;
use Codeception\PHPUnit\TestCase;
use InvalidArgumentException;

class OrderTest extends TestCase
{
    /** @var Order */
    private $order;

    protected function setUp(): void
    {
        $this->order = new Order(1, 2, [3, 4], 5);
    }

    /**
     * @test
     */
    public function constructorWrongId()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Wrong id');

        (new Order(-1, 1, [], 1));
    }

    /**
     * @test
     */
    public function constructorWrongStatus()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unknown status');

        (new Order(0, -1, [], 1));
    }

    /**
     * @test
     */
    public function getId()
    {
        $this->assertEquals(1, $this->order->getId());
    }

    /**
     * @test
     */
    public function setStatusWithException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unknown status');

        $this->order->setStatus(-1);
    }

    public function testGetStatus()
    {
        $this->assertEquals(2, $this->order->getStatus());
    }

    /**
     * @test
     */
    public function getItemsIds()
    {
        $this->assertEquals([3, 4], $this->order->getItemsIds());
    }

    /**
     * @test
     */
    public function getUserId()
    {
        $this->assertEquals(5, $this->order->getUserId());
    }
}