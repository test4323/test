<?php

namespace unit\App\Model\Order;

use App\Model\Order\Order;
use App\Model\Order\OrderRepository;
use App\Model\Order\PersistenceInterface;
use Codeception\PHPUnit\TestCase;
use Codeception\Stub;
use Throwable;

class OrderRepositoryTest extends TestCase
{
    /**
     * @test
     * @dataProvider saveDataProvider
     * @throws Throwable
     */
    public function save(Order $order, Order $expected)
    {
        /** @var PersistenceInterface $store */
        $store = Stub::makeEmpty(
            PersistenceInterface::class,
            [
                'insertOrder' => 1
            ]
        );

        $order = new Order(1, 1, [1], 1);

        $actual = (new OrderRepository($store))->save($order);


        $this->assertEquals($expected, $actual);
    }

    public function saveDataProvider()
    {
        return [
            [new Order(1, 1, [1], 1), new Order(1, 1, [1], 1)],
            [new Order(0, 1, [1], 1), new Order(1, 1, [1], 1)]

        ];
    }

    /**
     * @test
     * @dataProvider getByIdDataProvider
     * @throws \Exception
     */
    public function getById(int $id, ?array $orderData, array $orderItems, ?Order $expected)
    {
        /** @var PersistenceInterface $store */
        $store = Stub::makeEmpty(
            PersistenceInterface::class,
            [
                'getOrderById' => $orderData,
                'getItemsIdsById' => $orderItems
            ]
        );

        $actual = (new OrderRepository($store))->getById($id);
        $this->assertEquals($expected, $actual);
    }

    public function getByIdDataProvider()
    {
        return [
            [1, ['Status' => 2, 'UserId' => 3], [4, 5], new Order(1, 2, [4, 5], 3)],
            [1, null, [], null]
        ];
    }
}