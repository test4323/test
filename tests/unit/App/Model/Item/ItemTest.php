<?php

namespace unit\App\Model\Item;

use App\Model\Item\Item;
use Codeception\PHPUnit\TestCase;
use InvalidArgumentException;

class ItemTest extends TestCase
{
    /** @var Item */
    private $item;

    protected function setUp(): void
    {
        $this->item = new Item(2, 'name1', 3.45);
    }

    /**
     * @test
     */
    public function constructorWithException()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Wrong id');

        (new Item(-1, 'somename', 1.23));
    }

    /**
     * @test
     */
    public function fromArray()
    {
        $actual = Item::fromArray(['Id' => 2, 'Name' => 'name1', 'Price' => 3.45]);
        $expected = new Item(2, 'name1', 3.45);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     */
    public function getId()
    {
        $this->assertEquals(2, $this->item->getId());
    }

    public function testGetName()
    {
        $this->assertEquals('name1', $this->item->getName());
    }

    /**
     * @test
     */
    public function getPrice()
    {
        $this->assertEquals(3.45, $this->item->getPrice());
    }
}