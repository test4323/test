<?php

namespace unit\App\Model\Item;

use App\Model\Item\CompareItems;
use Codeception\PHPUnit\TestCase;

class CompareItemsTest extends TestCase
{
    /**
     * @test
     * @dataProvider compareIdsWithItemsDataProvider
     */
    public function compareIdsWithItems(array $ids, array $items, bool $expected)
    {
        $actual = (new CompareItems())->compareIdsWithItems($ids, $items);
        $this->assertEquals($expected, $actual);
    }

    public function compareIdsWithItemsDataProvider()
    {
        return [
            [[1, 2, 3], [1 => 'a', 2 => 'b', 3 => 'c'], true],
            [[1, 2, 3], [1 => 'a', 3 => 'c'], false],
            [[1, 2], [1 => 'a', 2 => 'b', 3 => 'c'], false]
        ];
    }
}