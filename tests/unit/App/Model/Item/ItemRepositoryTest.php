<?php

namespace unit\App\Model\Item;

use App\Model\Item\Item;
use App\Model\Item\ItemRepository;
use App\Model\Item\PersistenceInterface;
use Codeception\PHPUnit\TestCase;
use Codeception\Stub;
use Exception;

class ItemRepositoryTest extends TestCase
{
    /**
     * @test
     * @throws Exception
     */
    public function save()
    {
        /** @var PersistenceInterface $store */
        $store = Stub::makeEmpty(
            PersistenceInterface::class,
            [
                'insert' => 1
            ]
        );

        $actual = (new ItemRepository($store))->save(new Item(0, 'name1', 1.23));
        $expected = new Item(1, 'name1', 1.23);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @test
     * @throws Exception
     */
    public function getByIds()
    {
        /** @var PersistenceInterface $store */
        $store = Stub::makeEmpty(
            PersistenceInterface::class,
            [
                'findByIds' => [
                    ['Id' => 1, 'Name' => 'name1', 'Price' => 1.2],
                    ['Id' => 2, 'Name' => 'name2', 'Price' => 3.4],
                ]
            ]
        );

        $actual = (new ItemRepository($store))->getByIds([1, 2, 3]);
        $expected = [
            1 => new Item(1, 'name1', 1.2),
            2 => new Item(2, 'name2', 3.4)
        ];

        $this->assertEquals($expected, $actual);
    }
}