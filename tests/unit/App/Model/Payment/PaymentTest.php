<?php

namespace unit\App\Model\Payment;

use App\Model\Payment\Payment;
use App\Model\Payment\PaymentTransport;
use Codeception\PHPUnit\TestCase;
use Codeception\Stub;

class PaymentTest extends TestCase
{
    /**
     * @test
     * @dataProvider payDataProvider
     */
    public function pay(int $httpCode, bool $expected)
    {
        $transport = Stub::makeEmpty(
            PaymentTransport::class,
            [
                'getResult' => $httpCode
            ]
        );

        $actual = (new Payment($transport))->pay(1.234);
        $this->assertEquals($actual, $expected);
    }

    public function payDataProvider()
    {
        return [
            [200, true],
            [500, false]
        ];
    }
}