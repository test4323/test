<?php

namespace unit\App\Model\Mysql;

use App\Model\Mysql\Prep;
use Codeception\PHPUnit\TestCase;

class PrepTest extends TestCase
{
    public function testFields()
    {
        $prep = new Prep('isf', '?,?,?', [1, 'a', 2.3]);
        $this->assertEquals('isf', $prep->types);
        $this->assertEquals('?,?,?', $prep->placeholders);
        $this->assertEquals([1, 'a', 2.3], $prep->params);
    }
}