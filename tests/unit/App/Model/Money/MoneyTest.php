<?php

namespace unit\App\Model\Money;

use App\Model\Money\Money;
use Codeception\PHPUnit\TestCase;

class MoneyTest extends TestCase
{
    /**
     * @test
     * @dataProvider toMicroAmountDataProvider
     */
    public function toMicroAmount(float $amount, int $expected)
    {
        $actual = (new Money())->toMicroAmount($amount);

        $this->assertEquals($expected, $actual);
    }

    public function toMicroAmountDataProvider()
    {
        return [
            [1.23, 1230000],
            [1.2345678, 1234568]
        ];
    }
}