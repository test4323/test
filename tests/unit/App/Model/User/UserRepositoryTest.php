<?php

namespace unit\App\Model\User;

use App\Model\User\PersistenceInterface;
use App\Model\User\User;
use App\Model\User\UserRepository;
use Codeception\PHPUnit\TestCase;
use Codeception\Stub;
use Exception;

class UserRepositoryTest extends TestCase
{
    /**
     * @test
     * @dataProvider findByLoginDataProvider
     * @throws Exception
     */
    public function findByLogin(string $login, ?array $userData, ?User $expected)
    {
        /** @var PersistenceInterface $store */
        $store = Stub::makeEmpty(
            PersistenceInterface::class,
            [
                'findByLogin' => $userData
            ]
        );

        $actual = (new UserRepository($store))->findByLogin($login);

        $this->assertEquals($expected, $actual);
    }

    public function findByLoginDataProvider()
    {
        return [
            ['admin', ['Id' => 1, 'Login' => 'admin'], new User(1, 'admin')],
            ['user', null, null]
        ];
    }
}