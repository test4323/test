<?php

namespace unit\App\Model\User;

use App\Model\User\InMemoryPersistence;
use Codeception\PHPUnit\TestCase;

class InMemoryPersistenceTest extends TestCase
{
    /**
     * @test
     * @dataProvider findByLoginDataProvider
     */
    public function findByLogin(string $login, ?array $expected)
    {
        $actual = (new InMemoryPersistence())->findByLogin($login);

        $this->assertEquals($expected, $actual);
    }

    public function findByLoginDataProvider()
    {
        return [
            ['admin', ['Id' => 1, 'Login' => 'admin']],
            ['user', null]
        ];
    }
}