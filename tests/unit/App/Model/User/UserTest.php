<?php

namespace unit\App\Model\User;

use App\Model\User\User;
use Codeception\PHPUnit\TestCase;

class UserTest extends TestCase
{
    /** @var User */
    private $user;

    protected function setUp(): void
    {
        $this->user = new User(2, 'user_login');
    }

    /**
     * @test
     */
    public function getId()
    {
        $this->assertEquals(2, $this->user->getId());
    }

    /**
     * @test
     */
    public function getLogin()
    {
        $this->assertEquals('user_login', $this->user->getLogin());
    }

    /**
     * @test
     * @dataProvider fromArrayDataProvider
     */
    public function fromArray(array $data, User $expected)
    {
        $actual = User::fromArray($data);
        $this->assertEquals($expected, $actual);
    }

    public function fromArrayDataProvider()
    {
        return [
            [['Id' => 1, 'Login' => 'admin'], new User(1, 'admin')],
            [['Id' => 2, 'Login' => 'user'], new User(2, 'user')],
        ];
    }
}